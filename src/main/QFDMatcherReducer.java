import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;

import java.io.IOException;
import java.util.ArrayList;

public class QFDMatcherReducer extends Reducer<IntWritable, WebTrafficRecord, RequestReplyMatch, NullWritable> {

    @Override
    public void reduce(IntWritable key, Iterable<WebTrafficRecord> values,
                       Context ctxt) throws IOException, InterruptedException {

        // The input is a set of WebTrafficRecords for each key,
        // the output should be the WebTrafficRecords for
        // all cases where the request and reply are matched
        // as having the same
        // Source IP/Source Port/Destination IP/Destination Port
        // and have occured within a 10 second window on the timestamp.

        // One thing to really remember, the Iterable element passed
        // from hadoop are designed as READ ONCE data, you will
        // probably want to copy that to some other data structure if
        // you want to iterate mutliple times over the data.

        //System.err.println("Need to implement!");
        ArrayList<WebTrafficRecord> recordList0 = new ArrayList<WebTrafficRecord>();
        for (WebTrafficRecord value : values) {
            recordList0.add(new WebTrafficRecord(value));
        }
        
        ArrayList<WebTrafficRecord> recordList1 = new ArrayList<WebTrafficRecord>(recordList0);
        for (WebTrafficRecord record0: recordList0){
            for (WebTrafficRecord record1: recordList1){
                if (record0.tupleMatches(record1) && !record0.equals(record1) 
                        && (Math.abs(record0.getTimestamp() - record1.getTimestamp()) <= 10) 
                        && (((record0.getCookie() == null) && (record1.getUserName() == null)) ||
                                ((record1.getCookie() == null) && (record0.getUserName() == null)) )) {
                    RequestReplyMatch match;
                    if (record0.getUserName() == null) {
                        match = new RequestReplyMatch(record0, record1);
                    } else {
                        match = new RequestReplyMatch(record1, record0);
                    }
                    ctxt.write(match, NullWritable.get());
                    recordList1.remove(record1);
                    recordList0.remove(record1);
                    break;
                }
            }
            recordList1.remove(record0);
            recordList0.remove(record0);
        }
        
        
        // ctxt.write should be RequestReplyMatch and a NullWritable
    }
}
