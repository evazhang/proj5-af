import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class QFDWriterReducer extends Reducer<WTRKey, RequestReplyMatch, NullWritable, NullWritable> {

    @Override
    public void reduce(WTRKey key, Iterable<RequestReplyMatch> values,
                       Context ctxt) throws IOException, InterruptedException {

        // The input will be a WTR key and a set of matches.

        // You will want to open the file named
        // "qfds/key.getName()/key.getName()_key.getHashBytes()"
        // using the FileSystem interface for Hadoop.

        // EG, if the key's name is srcIP and the hash is 2BBB,
        // the filename should be qfds/srcIP/srcIP_2BBB

        // Some useful functionality:

        // FileSystem.get(ctxt.getConfiguration())
        // gets the interface to the filesysstem
        // new Path(filename) gives a path specification
        // hdfs.create(path, true) will create an
        // output stream pointing to that file
        HashSet<RequestReplyMatch> valueSet = new HashSet<RequestReplyMatch>();
        for (RequestReplyMatch value : values) {
            valueSet.add(new RequestReplyMatch(value));
        }

        String keyName = key.getName();
        String keyHashBytes = key.getHashBytes();
        QueryFocusedDataSet qfd = new QueryFocusedDataSet(keyName, keyHashBytes, valueSet);
        
        FileSystem hdfs = FileSystem.get(ctxt.getConfiguration());
        
        String filename = "qfds/" + keyName + "/" + keyName + "_" + keyHashBytes;
        FSDataOutputStream fileOut = hdfs.create(new Path(filename), true);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(qfd);
        fileOut.close();
        out.close();
    }
}
